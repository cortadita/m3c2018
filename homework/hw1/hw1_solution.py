"""M3C 2018 Homework 1 solution code

"""
import numpy as np
import matplotlib.pyplot as plt


def simulate1(N,Nt,b,e,S=0):
    """Simulate C vs. M competition on N x N grid over
    Nt generations. b and e are model parameters
    to be used in fitness calculations
    Output: S: Status of each gridpoint at tend of somulation, 0=M, 1=C
    fc: fraction of villages which are C at all Nt+1 times
    Do not modify input or return statement without instructor's permission.
    """
    #Set initial condition
    if S==0:
        S  = np.ones((N,N),dtype=int) #Status of each gridpoint: 0=M, 1=C
        j = int((N-1)/2)
        S[j-1:j+2,j-1:j+2] = 0
    N2inv = 1./(N*N)

    fc = np.zeros(Nt+1) #Fraction of points which are C
    fc[0] = S.sum()*N2inv

    #Initialize matrices
    NB = np.zeros((N,N),dtype=int) #Number of neighbors for each point
    NC = np.zeros((N,N),dtype=int) #Number of neighbors who are Cs
    S2 = np.zeros((N+2,N+2),dtype=int) #S + border of zeros
    F = np.zeros((N,N)) #Fitness matrix
    F2 = np.zeros((N+2,N+2)) #Fitness matrix + border of zeros
    A = np.ones((N,N)) #Fitness parameters, each of N^2 elements is 1 or b
    P = np.zeros((N,N)) #Probability matrix
    Pden = np.zeros((N,N))
    R = np.random.rand(N,N,Nt) #Random numbers used to update S every time step
    #---------------------

    #Calculate number of neighbors for each point
    NB[:,:] = 8
    NB[0,1:-1],NB[-1,1:-1],NB[1:-1,0],NB[1:-1,-1] = 5,5,5,5
    NB[0,0],NB[-1,-1],NB[0,-1],NB[-1,0] = 3,3,3,3
    NBinv = 1.0/NB
    #-------------

    #----Time marching-----
    for t in range(Nt):

        #Set up coefficients for fitness calculation
        A = np.ones((N,N))
        ind0 = np.where(S==0)
        A[ind0] = b

        #Add boundary of zeros to S
        S2[1:-1,1:-1] = S

        #Count number of C neighbors for each point
        NC = S2[:-2,:-2]+S2[:-2,1:-1]+S2[:-2,2:]+S2[1:-1,:-2] + S2[1:-1,2:] + S2[2:,:-2] + S2[2:,1:-1] + S2[2:,2:]

        #Calculate fitness matrix
        F = NC*A
        F[ind0] = F[ind0] + (NB[ind0]-NC[ind0])*e
        F = F*NBinv

        #Calculate probability matrix
        F2[1:-1,1:-1]=F
        F2S2 = F2*S2
        P = F2S2[:-2,:-2]+F2S2[:-2,1:-1]+F2S2[:-2,2:]+F2S2[1:-1,:-2] + F2S2[1:-1,1:-1] + F2S2[1:-1,2:] + F2S2[2:,:-2] + F2S2[2:,1:-1] + F2S2[2:,2:]
        Pden = F2[:-2,:-2]+F2[:-2,1:-1]+F2[:-2,2:]+F2[1:-1,:-2] + F2[1:-1,1:-1] + F2[1:-1,2:] + F2[2:,:-2] + F2[2:,1:-1] + F2[2:,2:]
        P = P/Pden

        #Set new affiliations based on probability matrix and random numbers stored in R
        #Some/many students will have used np.random.random_choice instead
        S[:,:] = 0
        S[R[:,:,t]<=P] = 1

        fc[t+1] = S.sum()*N2inv
        #plot_S(S)
        #----Finish time marching-----

    return S,fc,P


def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #C locations
    ind_s1 = np.where(S==1) #M locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.hold(True)
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.hold(False)
    plt.show()
    plt.pause(0.05)
    return None


def simulate2(N,Nt,bvalues,e,M,S=0):
    """N,Nt,e are the same as in simulate1
    M simulations are run for each of the values of b stored in bvalues,
    and fc from all simulations are returned to analyze for further... analysis.
    """
    Nb = len(bvalues)
    bv = np.array(bvalues)

    #Set initial condition
    if S==0:
        S  = np.ones((N,N,M,Nb),dtype=int) #Status of each gridpoint: 0=M, 1=C
        j = int((N-1)/2)
        S[j-1:j+2,j-1:j+2,:,:] = 0
    N2inv = 1./(N*N)

    fc = np.zeros((Nt+1,M,Nb)) #Fraction of points which are C
    fc[0,:,:] = S.sum(axis=(0,1))*N2inv

    #Initialize matrices
    NB = np.zeros((N,N,M,Nb),dtype=int) #Number of neighbors for each point
    NC = np.zeros((N,N,M,Nb),dtype=int) #Number of neighbors who are Cs
    S2 = np.zeros((N+2,N+2,M,Nb),dtype=int) #S + border of zeros
    F = np.zeros((N,N,M,Nb)) #Fitness matrix
    F2 = np.zeros((N+2,N+2,M,Nb)) #Fitness matrix + border of zeros
    A = np.ones((N,N,M,Nb)) #Fitness parameters, each of N^2 elements is 1 or b
    P = np.zeros((N,N,M,Nb)) #Probability matrix
    Pden = np.zeros((N,N,M,Nb))
    #---------------------

    #Calculate number of neighbors for each point
    NB[:,:,:,:] = 8
    NB[0,1:-1,:,:],NB[-1,1:-1,:,:],NB[1:-1,0,:,:],NB[1:-1,-1,:,:] = 5,5,5,5
    NB[0,0,:,:],NB[-1,-1,:,:],NB[0,-1,:,:],NB[-1,0,:,:] = 3,3,3,3
    NBinv = 1.0/NB
    #-------------

    #----Time marching-----
    for t in range(Nt):
        R = np.random.rand(N,N,M,Nb) #Random numbers used to update S every time step

        #Set up coefficients for fitness calculation
        A = np.ones((N,N,M,Nb))
        ind0 = np.where(S==0)
        A[ind0] = bv[ind0[3]]

        #Add boundary of zeros to S
        S2[1:-1,1:-1,:,:] = S

        #Count number of C neighbors for each point
        NC = S2[:-2,:-2,:,:]+S2[:-2,1:-1,:,:]+S2[:-2,2:,:,:]+S2[1:-1,:-2,:,:] + S2[1:-1,2:,:,:] + S2[2:,:-2,:,:] + S2[2:,1:-1,:,:] + S2[2:,2:,:,:]

        #Calculate fitness matrix
        F = NC*A
        F[ind0] = F[ind0] + (NB[ind0]-NC[ind0])*e
        F = F*NBinv

        #Calculate probability matrix
        F2[1:-1,1:-1,:,:]=F
        F2S2 = F2*S2
        P = F2S2[:-2,:-2,:,:]+F2S2[:-2,1:-1,:,:]+F2S2[:-2,2:,:,:]+F2S2[1:-1,:-2,:,:] + F2S2[1:-1,1:-1,:,:] + F2S2[1:-1,2:,:,:] + F2S2[2:,:-2,:,:] + F2S2[2:,1:-1,:,:] + F2S2[2:,2:,:,:]
        Pden = F2[:-2,:-2,:,:]+F2[:-2,1:-1,:,:]+F2[:-2,2:,:,:]+F2[1:-1,:-2,:,:] + F2[1:-1,1:-1,:,:] + F2[1:-1,2:,:,:] + F2[2:,:-2,:,:] + F2[2:,1:-1,:,:] + F2[2:,2:,:,:]
        P = P/Pden

        #Set new affiliations based on probability matrix and random numbers stored in R
        #Some/many students will have used np.random.random_choice instead
        S[:,:,:,:] = 0
        S[R<=P] = 1

        fc[t+1,:,:] = S.sum(axis=(0,1))*N2inv
        #plot_S(S)
        #----Finish time marching-----

    return S,fc



def analyze(N,Nt,bvalues,e,M):
    """ For each of the bvalues, M simulations are run and figures are
    constructed using the computed fc arrays
    """

    S,fc = simulate2(N,Nt,bvalues,e,M)
    fm = fc.mean(axis=1)

    plt.figure()
    plt.plot(fc[:,::15,4],'--')
    plt.plot(fm[:,4],'k-',linewidth=2)
    plt.xlim([0,150])
    plt.xlabel('t')
    plt.ylabel('$f_c$')
    plt.title('Sample simulations (dashed) and mean (solid black) with b=%f' %(bvalues[4]))

    plt.figure()
    plt.plot(fm)
    plt.xlabel('t')
    plt.ylabel('$<f_c>$')
    plt.title('Average of fc over %d simulations, %3.2f<=b<=%3.2f \n N=%d, e=%d' %(M,np.min(bvalues),np.max(bvalues),N,e))

    plt.figure()
    plt.plot(bvalues,fm[-1,:])
    plt.xlabel('b')
    plt.ylabel('$<f_c>$')
    plt.title('Average of fc(t=%d) over %d simulations \n N=%d, e=%d' %(Nt,M,N,e))

    return None

if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    bvalues = np.linspace(1,1.5,11); bvalues[0]=1.01
    N,Nt,e,M = 21,800,0.01,100
    output = analyze(N,Nt,bvalues,e,M)