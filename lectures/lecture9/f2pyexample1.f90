!--------------------------------------------------------------------
!function to sum two numbers provided as input
!note that the function name is a variable whose
!type must be declared
!- the function is returned (output)

!f2py -c f2pyexample1.f90 -m s1 to compile with f2py - check no errors
!.so: shared object file
!import s1 inside python

!create new module when adding/recompiling
!same way to import/call functions and subroutines

function sumxy(x,y)
    implicit none
    double precision, intent(in) :: x,y
    double precision :: sumxy

    sumxy = x + y  !output always name of the function
end function sumxy

!--------------------------------------------------------------------
!--------------------------------------------------------------------
!subroutine to sum two numbers provided as input
subroutine sumxy2(x,y,sumxy)
    implicit none
    double precision, intent(in) :: x,y
    double precision, intent(out) :: sumxy  !important to define for py output

    sumxy = x + y
    print *, 'test3'
end subroutine sumxy2

! s = sumxy(a,b) - for function
! call sumxy2(a,b,s) - for subroutine
