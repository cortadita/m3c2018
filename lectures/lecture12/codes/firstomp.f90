!Getting started with OpenMP
!To compile: gfortran -fopenmp -o first.exe firstomp.f90
!To run: $ ./first.exe
program firstomp
	use omp_lib !makes OpenMP routines, variables available
	implicit none
	integer :: NumThreads,threadID

! creating parallel region
! openMP has its own functions too
! number of threads shown can be hyperthreads (e.g. 4 instead of 2)
! private means each thread gets its copy of the variable - own ID (random order)
! otherwise threads can overwrite values from other threads (threadID shared var)
! - threads share memory in diff cores

!$OMP PARALLEL PRIVATE(threadID)
	NumThreads = omp_get_num_threads()
	threadID = omp_get_thread_num()
	print *, 'this is thread',threadID, ' of ', NumThreads
!$OMP END PARALLEL

    print *, 'exited parallel region, this is thread', threadID

end program firstomp
