program loop
implicit none

integer :: i1, threadID, NumThreads
real(kind=8), dimension(12) :: x, y,z

!initialise arrays
call random_number(y)
call random_number(z)

!computer x=y+z
x = y + z

!check x-y-z = 0
print *, 'check:',maxval(abs(x-y-z))

end program loop
