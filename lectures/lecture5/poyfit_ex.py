#%% lecture 5

#%% simple polyfit example (linear least squares with numpy)

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0,1,100)
T = 2*x + 3 + 0.25*np.random.randn(100)

C = np.polyfit(x,T,1)  # 1 = degree of fitting polynomial
print("C1=",C[0],", C2=",C[1])

Tf = C[0]*x + C[1]

plt.plot(Tf,'.',)
plt.plot(T,'+')
plt.legend(['Polyfit','Data'])

# np.linalg.lstsq for linear least squres as well
